﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VAERecordPlugin : MonoBehaviour
{
    public int frequency = 44100;
    public int timeForRecording = 3600;

    public Button buttonStartRecord, buttonStopRecord;
    public Button buttonStartPlay, buttonStopPlay;

    private string _micName;
    private int _frequency = 16000;
    private float _time = 0f;
    private int _startSample = 0;

    private AudioSource _sourceRecording;
    private AudioClip _currentClip;

    void Awake()
    {
        _sourceRecording = GetComponent<AudioSource>();
    }

    public void ClickButtonStartRecord()
    {
        if (buttonStartRecord != null)
            buttonStartRecord.gameObject.SetActive(false);
        if (buttonStopRecord != null)
            buttonStopRecord.gameObject.SetActive(true);
        StartRecording();
    }

    public void ClickButtonStopRecord()
    {
        if (buttonStartRecord != null)
            buttonStartRecord.gameObject.SetActive(true);
        if (buttonStopRecord != null)
            buttonStopRecord.gameObject.SetActive(false);
        StopRecording();
    }

    public void ClickButtonStartPlay()
    {
        if (buttonStartPlay != null)
            buttonStartPlay.gameObject.SetActive(false);
        if (buttonStopPlay != null)
            buttonStopPlay.gameObject.SetActive(true);
        StartPlaying();
    }

    public void ClickButtonStopPlay()
    {
        if (buttonStartPlay != null)
            buttonStartPlay.gameObject.SetActive(true);
        if (buttonStopPlay != null)
            buttonStopPlay.gameObject.SetActive(false);
        StopPlaying();
    }

    private void StartRecording()
    {
        if (Microphone.devices.Length < 1)
        {
            Debug.LogWarning("Microphone was not find");
            return;
        }

        _micName = Microphone.devices[0];        
#if UNITY_ANDROID
        int minFreq, maxFreq;
        Microphone.GetDeviceCaps(_micName, out minFreq, out maxFreq);
        minFreq = Mathf.Max(minFreq, 100);
        maxFreq = Mathf.Max(maxFreq, 100);
        _frequency = Mathf.Clamp(frequency, minFreq, maxFreq);
#else
        _frequency = Mathf.Clamp(frequency, 100, 44100);
#endif

        _sourceRecording.clip = Microphone.Start(_micName, false, timeForRecording, _frequency);

        _startSample = (int) Time.time;
    }

    private void StopRecording()
    {
        if (Microphone.devices.Length < 1)
            return;

        int samplesCount = (int) Time.time - _startSample;

        if (samplesCount > 0)
        {
            float[] samples = new float[samplesCount];
            _sourceRecording.clip.GetData(samples, _startSample);
            AudioClip clip = AudioClip.Create("audio", samplesCount, _sourceRecording.clip.channels, _frequency, false);
            clip.SetData(samples, 0);            
        }

        Microphone.End(_micName);
        _sourceRecording.Stop();
    }

    private void StartPlaying()
    {
        _sourceRecording.Stop();

        _sourceRecording.PlayOneShot(_sourceRecording.clip);
    }

    private void StopPlaying()
    {
        _sourceRecording.Stop();
    }
}
